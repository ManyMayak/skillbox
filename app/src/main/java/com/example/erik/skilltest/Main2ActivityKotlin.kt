package com.example.erik.skilltest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

lateinit var chat:TextView
lateinit var message:EditText
lateinit var send:Button
lateinit var clean:Button




class Main2ActivityKotlin : AppCompatActivity(), View.OnClickListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2_kotlin)

        chat=findViewById(R.id.chat) as TextView
        message=findViewById(R.id.message) as EditText
        send=findViewById(R.id.send) as Button
        clean=findViewById(R.id.clean) as Button

        send.setOnClickListener(this)
        clean.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.send->{
                var question= message.text.toString()
                message.setText("")
                chat.append("\n<< " + question)
                var answer=answerQuestion(question)
                chat.append("\n>> " + answer)
            }
            R.id.clean->{
                chat.setText(R.string.welcome)
            }
        }
    }

    fun answerQuestion(question: String):String{
        var question=question.toLowerCase()
        val questions:MutableMap<String,String> = mutableMapOf()
        questions.put("как дела", "Шикарно")
        questions.put("чем занимаешься", "Отвечаю на дурацкие вопросы")
        questions.put("как тебя зовут", "Меня зовут Ассистентий")
        questions.put("лал", "кек")
        questions.put("кек", "чебурек")
        questions.put("в чем смысл жизни","Cмысл в том, чтобы кодить sdfsd")
        val result:MutableList<String> = mutableListOf()
        for (quest:String in questions.keys) {
            if (question.contains(quest)) {
                result.add(questions.get(quest).toString())
            }
        }

        if(question.contains("сколько времени")){
            var fmt:DateFormat=SimpleDateFormat("HH:mm:ss")
            var time=fmt.format(Date())
            result.add("Сейчас "+time)
        }
        if(question.contains("сколько времени")){
            var fmt:DateFormat=SimpleDateFormat("EEEE dd MMMM")
            var time=fmt.format(Date())
            result.add("Сегодня "+time)
        }

        return TextUtils.join(",", result)
    }
}
