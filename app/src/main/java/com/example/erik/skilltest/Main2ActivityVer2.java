package com.example.erik.skilltest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main2ActivityVer2 extends AppCompatActivity implements View.OnClickListener {

    protected TextView chat;

    /**
     * Поле для ввода сообщение
     */
    protected EditText message;

    /**
     * Кнопка "отправить"
     */
    protected Button send,cleanChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chat = (TextView) findViewById(R.id.chat);
        message = (EditText) findViewById(R.id.message);
        send = (Button) findViewById(R.id.send);
        cleanChat = (Button) findViewById(R.id.clean);

        /**
         * Назначим обработчик события "Клик" на кнопке "отправить"
         */
        send.setOnClickListener(this);
        cleanChat.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.send:
                String question = message.getText().toString(); // Получим текст вопроса
                message.setText(""); // Очистим поле
                chat.append("\n<< " + question); // Отобразим вопрос в чате
                String answer = answerQuestion(question); // Вычислим тексто ответа
                chat.append("\n>> " + answer); // Отобразим ответ в чате
                break;
            case R.id.clean:
                chat.setText(R.string.welcome);
                break;
        }
    }


    protected String answerQuestion(String question) {
        question = question.toLowerCase();
        Map<String, String> questions = new HashMap<String, String>(){{
            put("как дела", "Шикарно");
            put("чем занимаешься", "Отвечаю на дурацкие вопросы");
            put("как тебя зовут", "Меня зовут Ассистентий");
            put("лал", "кек");
            put("кек", "чебурек");
            put("в чем смысл жизни","Cмысл в том, чтобы кодить");
        }};

        List<String> result = new ArrayList<>();

        for(String quest : questions.keySet()) {
            if (question.contains(quest)) {
                result.add(questions.get(quest));
            }
        }
        DateFormat fmt;
        String time;
        String match="default";
        String[] possibleQuest={"сколько времени","какой сегодня день"};
        for(int i=0;i<possibleQuest.length; i++){
            if(question.contains(possibleQuest[i])){
                match=possibleQuest[i];
                switch (match){
                    case "сколько времени":
                        fmt = new SimpleDateFormat("HH:mm:ss");
                        time= fmt.format(new Date());
                        result.add("Сейчас " + time);
                        break;
                    case "какой сегодня день":
                        fmt = new SimpleDateFormat("EEEE dd MMMM");
                        time = fmt.format(new Date());
                        result.add("Сегодня " + time);
                        break;
                }
            }
        }




        return TextUtils.join(", ", result); // Все получившиеся ответы объединим через запятую и вернем как результат метода
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */

}
